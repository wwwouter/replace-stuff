/// <reference types="bluebird" />
import * as Promise from 'bluebird';
import { IOptions, IInputReplacement, IReplacement } from './interfaces';
export default class Replacer {
    private gitSha;
    private includes;
    private excludes;
    private flags;
    constructor(options?: IOptions);
    replace(globPattern: string, folderPath: string, replacements: IInputReplacement[], options?: IOptions): Promise<{}>;
    replaceFiles(globPattern: string, folderPath: string, replacements: IReplacement[], options?: IOptions): Promise<{}>;
    replaceText(input: string, search: string, replacement: string): string;
    matches(input: string, search: string): RegExpMatchArray | null;
    replaceTextInFile(filename: string, replacements: IReplacement[], outputFilename?: string): Promise<any>;
    getFilenames(globPattern: string, folderPath: string): string[];
    parseReplacements(replacements: IInputReplacement[]): IReplacement[];
    private includeFile(file, isFile);
    private parseReplacement(inputReplacement);
    private getGitSha();
    private parseSearchString(search);
    private readFile(filename);
    private writeFile(filename, data);
}
