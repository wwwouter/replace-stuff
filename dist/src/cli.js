"use strict";
const minimist = require('minimist');
const fs = require('fs-extra');
const Replacer_1 = require('./Replacer');
let argv = minimist(process.argv.slice(2));
if (argv.h || argv.help) {
    console.log('Usage: replacestuff search replace globPattern [--output=dist/] [--basedir=build/] [--include=*.js] [--exclude=*.backup] [--excludeList=exclude.txt]');
    process.exit(0);
}
let options = {};
if (argv.include) {
    if (argv.include instanceof Array) {
        options.includes = argv.include;
    }
    else {
        options.includes = [argv.include];
    }
}
if (argv.exclude) {
    if (argv.exclude instanceof Array) {
        options.excludes = argv.exclude;
    }
    else {
        options.excludes = [argv.exclude];
    }
}
if (argv.excludeList) {
    options.excludeList = argv.excludeList;
}
if (argv.o || argv.output) {
    options.outputPath = argv.o || argv.output;
}
let baseDir = process.cwd();
if (argv.basedir) {
    baseDir = argv.basedir;
}
let strings = argv['_'];
let replacements = [];
let globPattern;
if (strings.length === 3) {
    let search = strings[0];
    let replace = strings[1];
    globPattern = strings[2];
    replacements.push({ searchRegExp: search, replace: replace });
}
else {
    globPattern = strings[0];
    let replacementsFilename = argv.replacements || './replacestuff.json';
    if (fs.existsSync(replacementsFilename)) {
        replacements = fs.readJsonSync(replacementsFilename);
    }
    else {
        console.error(`Cannot find '${replacementsFilename}'`);
        process.exit(1);
    }
}
let replacer = new Replacer_1.default(options);
replacer
    .replace(globPattern, baseDir, replacements, options)
    .then(() => console.log('Replaced successful.'))
    .catch((error) => {
    console.error(error);
    process.exit(1);
});
//# sourceMappingURL=cli.js.map