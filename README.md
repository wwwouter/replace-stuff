
# replacestuff

[![Build Status](https://gitlab.com/wwwouter/replace-stuff/badges/master/build.svg)](https://gitlab.com/wwwouter/replace-stuff/)
[![npm version](https://badge.fury.io/js/replacestuff.svg)](https://badge.fury.io/js/replacestuff)

[![npm](https://nodei.co/npm/replacestuff.png?stars&downloads&downloadRank)](https://nodei.co/npm/replacestuff.png?stars&downloads&downloadRank)


Replaces strings in files. Useful creating a npm only build pipeline. Inspired by https://github.com/harthur/replace .

Strings can be replaced by other strings, environment variables and the current git version hash (SHA).

Does not follow symlinks.



## Installing

    npm install -g replacestuff

## Using


In-place replace:

    replacestuff s1 s2 dist/**/*.html

Different output folder

    replacestuff s1 s2 **/* --basedir=build --output=dist/

## Options

    replacestuff search replace globPattern [--output=dist/] [--basedir=build/] [--include=*.js] [--exclude=*.backup] [--excludeList=exclude.txt]');

* search: string to search
* replace: string to replace matches with
* globPattern: glob pattern to look for files
* --output: directory where output files will be written (if empty, it will be in-place)
* --basedir: directory that is used as root for globPattern


### With replacestuff.json configuration

If there are several strings to replace, it's better to use a `replacestuff.json` file.

    [
        { search: 'search0', replace: 'replace0' },
        { searchRegExp: 'search1', replace: 'replace1' },
        { search: 'search2', replaceVar: 'gitsha' },
        { search: 'search3', replaceEnv: 'replace3' }
    ]


In this example
* the string 'search' will be replaced by 'replace0'.
* strings matching the regular expression 'search1' will be replaced by 'replace1'.
* the string 'search2' will be replaced by the current git version hash.
* the string 'search3' will be replaced by the value of the environment variable 'replace3'.

Keep in mind that each replacement happens in the same order as in `replacestuff.json`.

## Test

npm test

## Develop

npm run dev:watch

## Update version

    npm version major|minor|patch
    npm publish
    git push

## Tslint

rules
- https://github.com/palantir/tslint
- https://github.com/Microsoft/tslint-microsoft-contrib/

## [License](LICENSE)

Copyright (c) 2016 Wouter Mooij.

Licensed under the [MIT License](LICENSE).