import * as tmp from 'tmp';
import * as fs from 'fs';
import * as path from 'path';
import Replacer from '../src/Replacer';
import {IInputReplacement } from '../src/interfaces';
import * as chai from 'chai';
import * as chaiFs from 'chai-fs';
chai.use(chaiFs);
let assert = chai.assert;
let assertFileContent = (assert as any).fileContent;

describe('Replacer', () => {
  it('should replace text', (done) => {
    let replacer = new Replacer();
    let originalText = 'This is a test.\nThis is a test.';
    let replacedText = replacer.replaceText(originalText, 'test', 'SUCCESSSSS');

    assert.equal(replacedText, 'This is a SUCCESSSSS.\nThis is a SUCCESSSSS.');

    done();
  });

  it('should replace text in file', () => {
    let replacer = new Replacer();
    let originalText = 'This is a t\\/!@#$%^&*()est.\nThis is a t\\/!@#$%^&*()est.';
    let filename = tmp.fileSync().name;
    fs.writeFileSync(filename, originalText);

    return replacer.replaceTextInFile(filename, replacer.parseReplacements([{ search: 't\\/!@#$%^&*()est', replace: 'SUCCESSSSS' }]))
      .then(() => {
        assertFileContent(filename, 'This is a SUCCESSSSS.\nThis is a SUCCESSSSS.', 'File contents not replaced.');
      });
  });

  it('should get files from glob', (done) => {
    let replacer = new Replacer();

    let tmpDir = tmp.dirSync();
    fs.writeFileSync(path.join(tmpDir.name, 'file1.ext'), '');
    fs.writeFileSync(path.join(tmpDir.name, 'file2.ext'), '');
    fs.writeFileSync(path.join(tmpDir.name, 'file2.no'), '');
    let filenames = replacer.getFilenames('*.ext', tmpDir.name);

    assert.equal(filenames.length, 2);

    done();
  });

  it('should create new file', () => {
    let replacer = new Replacer();

    let inputDir = tmp.dirSync();
    let outputDir = tmp.dirSync();
    fs.writeFileSync(path.join(inputDir.name, 'file1.ext'), 'test');

    return replacer.replaceFiles('*', inputDir.name, [{ search: 'test', replace: 'SUCCESSSSS' }], { outputPath: outputDir.name })
      .then(() => {
        assertFileContent(path.join(outputDir.name, 'file1.ext'), 'SUCCESSSSS');

      });
  });

  it('should parse replacements', (done) => {
    let replacer = new Replacer();
    let replacements = [
      { search: 'search0', replace: 'replace0' },
      { searchRegExp: 'search1', replace: 'replace1' },
      { search: 'search2', replaceVar: 'gitsha' },
      { search: 'search3', replaceEnv: 'replace3' }
    ] as IInputReplacement[];
    process.env.replace3 = 'ENV';


    let parsedReplacements = replacer.parseReplacements(replacements);

    assert.equal(parsedReplacements[0].search, replacements[0].search);
    assert.isNotNull(parsedReplacements[1].search);
    assert.notEqual(parsedReplacements[2].replace, 'gitsha');
    assert.equal(parsedReplacements[3].replace, 'ENV');

    done();
  });

  it('should ignore files from defaultignore list', (done) => {
    let replacer = new Replacer();

    let tmpDir = tmp.dirSync();
    fs.writeFileSync(path.join(tmpDir.name, 'file1.ext'), '');
    fs.writeFileSync(path.join(tmpDir.name, 'file2.ext'), '');
    fs.writeFileSync(path.join(tmpDir.name, 'file3.ogg'), '');
    let filenames = replacer.getFilenames('*', tmpDir.name);

    assert.equal(filenames.length, 2);

    done();
  });
});