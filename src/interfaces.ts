
export interface IOptions {
    outputPath?: string;
    includes?: string[];
    excludes?: string[];
    excludeList?: string;
    ignoreCase?: boolean;
    multiline?: boolean;
}

export interface IReplacement {
    search: string;
    replace: string;
}

export interface IInputReplacement {
    search?: string;
    searchRegExp?: string;
    replace?: string;
    replaceVar?: string;
    replaceEnv?: string;
}

