import * as fs from 'fs-extra';
import * as path from 'path';
import * as Promise from 'bluebird';
import * as glob from 'glob';
import * as underscore from 'underscore';
import * as child_process from 'child_process';
import * as minimatch from 'minimatch';
import { IOptions, IInputReplacement, IReplacement } from './interfaces';


export default class Replacer {

    private gitSha: string;
    private includes: string[];
    private excludes: string[];

    private flags: string;



    constructor(options?: IOptions) {
        if (options && options.includes) {
            this.includes = options.includes;
        }
        this.excludes = [];
        if (options && options.excludes) {
            this.excludes = options.excludes;
        }
        let ignoreFile = (options && options.excludeList) || path.join(__dirname, '/defaultignore');
        let ignores = fs.readFileSync(ignoreFile, 'utf-8').split('\n');
        this.excludes = this.excludes.concat(ignores);

        this.flags = 'g';
        if (options) {
            if (options.ignoreCase) {
                this.flags += 'i';
            }
            if (options.multiline) {
                this.flags += 'm';
            }
        }
    }

    public replace(globPattern: string, folderPath: string, replacements: IInputReplacement[], options?: IOptions): Promise<{}> {
        return this.replaceFiles(globPattern, folderPath, this.parseReplacements(replacements), options);
    }

    public replaceFiles(globPattern: string, folderPath: string, replacements: IReplacement[], options?: IOptions): Promise<{}> {
        let filenames = this.getFilenames(globPattern, folderPath);

        return Promise.map(filenames, (filename) => {
            let outputFilename = undefined as string | undefined;
            if (options && options.outputPath) {
                outputFilename = path.join(options.outputPath, filename.substring(folderPath.length + 1));
            }
            return this.replaceTextInFile(filename, replacements, outputFilename);
        });
    }

    public replaceText(input: string, search: string, replacement: string): string {
        return input.replace(new RegExp(search, this.flags), replacement);
    }

    public matches(input: string, search: string): RegExpMatchArray | /* tslint:disable */null/* tslint:enable */ {
        return input.match(new RegExp(search, this.flags));
    }



    public replaceTextInFile(filename: string, replacements: IReplacement[], outputFilename?: string): Promise<any> {
        return this.readFile(filename)
            .then((text) => {
                let replacedText = text;
                let hasMatch = false;

                replacements.forEach((replacement) => {
                    let matches = this.matches(replacedText, replacement.search);
                    if (matches && matches.length !== 0) {
                        hasMatch = true;
                        replacedText = this.replaceText(replacedText, replacement.search, replacement.replace);
                    }
                });
                if (hasMatch === false) {
                    return Promise.resolve(undefined);
                }
                if (outputFilename) {
                    fs.ensureFileSync(outputFilename);
                    return this.writeFile(outputFilename, replacedText);
                }
                return this.writeFile(filename, replacedText);
            });
    }

    public getFilenames(globPattern: string, folderPath: string): string[] {
        let result = [] as string[];
        result = glob.sync(globPattern, { cwd: folderPath });

        result = underscore.map(result, (filename) => path.join(folderPath, filename));
        result = underscore.filter(result as any, (filename: string): boolean => {
            let fileStat = fs.statSync(filename);
            return fileStat.isSymbolicLink() === false && fileStat.isFile() && this.includeFile(filename, fileStat.isFile());
        });
        return result;
    }

    public parseReplacements(replacements: IInputReplacement[]): IReplacement[] {
        let result = [] as IReplacement[];
        replacements.forEach((replacement) => {
            result.push(this.parseReplacement(replacement));
        });

        return result;
    }

    private includeFile(file: string, isFile: boolean) {
        let inIncludes = this.includes && this.includes.some((include) => {
            return minimatch(file, include, { matchBase: true });
        });
        let inExcludes = this.excludes.some((exclude) => {
            return minimatch(file, exclude, { matchBase: true });
        });

        return ((!this.includes || !isFile || inIncludes) && (!this.excludes || !inExcludes));
    }

    private parseReplacement(inputReplacement: IInputReplacement): IReplacement {
        let search = inputReplacement.search ? this.parseSearchString(inputReplacement.search) : inputReplacement.searchRegExp;
        let replace = inputReplacement.replace;

        if (inputReplacement.replaceEnv) {
            replace = process.env[inputReplacement.replaceEnv] ? this.parseSearchString(process.env[inputReplacement.replaceEnv]) : '';
        } else if (inputReplacement.replaceVar) {
            switch (inputReplacement.replaceVar) {
                case 'gitsha':
                    replace = this.parseSearchString(this.getGitSha());
                    break;
                default:
                    throw `Variable '${inputReplacement.replaceVar} is unknown`;
            }
        }

        return {
            search: search || '',
            replace: replace || ''
        };
    }


    private getGitSha(): string {
        if (this.gitSha) {
            return this.gitSha;
        }
        this.gitSha = child_process.execSync('git rev-parse HEAD').toString().trim();

        return this.gitSha;
    }

    private parseSearchString(search: string): string {
        // http://stackoverflow.com/questions/1144783/replacing-all-occurrences-of-a-string-in-javascript
        return search.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
    }

    private readFile(filename: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            fs.readFile(filename, 'utf-8', (error, result) => {
                if (error) {
                    return reject(error);
                }
                return resolve(result);
            });
        });
    }

    private writeFile(filename: string, data: string): Promise<{}> {
        return new Promise<{}>((resolve, reject) => {
            fs.writeFile(filename, data, (error: Error) => {
                if (error) {
                    return reject(error);
                }
                return resolve({});
            });
        });
    }

}